FROM rust:1.64

COPY Cargo.toml Cargo.toml
COPY Cargo.lock Cargo.lock
COPY src/ src/
COPY static/ static/

RUN cargo build --release

EXPOSE 8080

ENTRYPOINT ["./target/release/tank_game"]
