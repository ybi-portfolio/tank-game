use core::fmt::Debug;
use std::time::{Duration, Instant};
use actix_web_actors::ws::Message::*;
use actix::prelude::{Actor, ActorContext, ActorFutureExt, Addr, AsyncContext, ContextFutureSpawner, Handler, Running, StreamHandler, WrapFuture, fut};
use actix_web_actors::ws;
use serde::{Serialize, Deserialize};

use crate::server;
use crate::dto::{GameEvent, Disconnect, Connect, PlayerCommand, PlayerCommandWithIdentity};

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);

const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

pub struct WsClient {
    pub session_id: usize,
    pub last_heartbeat: Instant,
    pub server: Addr<server::GameServer>,
}

impl WsClient {
    fn init_heartbeat(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.last_heartbeat) > CLIENT_TIMEOUT {
                act.server.do_send(Disconnect { session_id: act.session_id });
                ctx.stop();
            } else {
                ctx.ping(b"");
            }
        });
    }
}

impl Actor for WsClient {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.init_heartbeat(ctx);

        let addr = ctx.address();
        self.server
            .send(Connect {
                subscriber: addr.recipient(),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(response) => {
                        act.session_id = response;
                        let identity = Self::to_json_or_warn(&GameEvent::Identity {
                            player_id: act.session_id
                        }).expect("unreachable");
                        ctx.text(identity);
                    },
                    _ => {
                        log::warn!("Server couldn't add player");
                        ctx.stop();
                    }
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        self.server.do_send(Disconnect { session_id: self.session_id });
        Running::Stop
    }
}

impl Handler<GameEvent> for WsClient {
    type Result = ();

    fn handle(&mut self, msg: GameEvent, ctx: &mut Self::Context) {
        if let Some(msg_json) = Self::to_json_or_warn(&msg) {
            ctx.text(msg_json);
        }
    }
}

type WsMessageOrError = Result<ws::Message, ws::ProtocolError>;
impl StreamHandler<WsMessageOrError> for WsClient {
    fn handle(&mut self, msg: WsMessageOrError, ctx: &mut Self::Context) {
        let msg = match msg {
            Err(err) => {
                log::warn!("Protocol error: {}", err);
                ctx.stop();
                return;
            }
            Ok(msg) => msg,
        };

        match msg {
            Ping(msg) => {
                self.last_heartbeat = Instant::now();
                ctx.pong(&msg);
            }
            Pong(_) => {
                self.last_heartbeat = Instant::now();
            }
            Text(text) => {
                let m = text.trim();
                self.handle_text_message(m);
            }
            Binary(_) => println!("Unexpected binary"),
            Close(_) | Continuation(_) => {
                ctx.stop();
            }
            Nop => (),
        }
    }
}

impl WsClient {
    fn to_json_or_warn<T: Serialize + Debug>(val: &T) -> Option<String> {
        let res = serde_json::to_string(val);
        if let Err(err) = &res {
            log::warn!("Failed to serialize value {:?}, error {}", val, err);
        }
        res.ok()
    }
    
    fn from_json_or_debug<T: for<'a> Deserialize<'a> + Debug>(val: &str) -> Option<T> {
        let res = serde_json::from_str(val);
        if let Err(err) = &res {
            log::debug!("Failed to deserialize value {:?}, error {}", val, err);
        }
        res.ok()
    }

    fn handle_text_message(&self, text: &str) {
        if let Some(command) = Self::from_json_or_debug::<PlayerCommand>(text) {
            self.server.do_send(PlayerCommandWithIdentity {
                player_id: self.session_id,
                command
            })
        }
    }
}
