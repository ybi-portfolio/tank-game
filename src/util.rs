use std::time::{SystemTime, UNIX_EPOCH};

use crate::algebra::Point2D;

pub fn get_epoch_time() -> u128 {
    let duration = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time not available :(");
    duration.as_millis()
}

pub fn does_circle_collide_with_rect(
    c_center: &Point2D,
    c_radius: f32,
    r_center: &Point2D,
    r_width: f32,
    r_height: f32
) -> bool {
    let Point2D { x: cx, y: cy } = *c_center;
    let Point2D { x: rx, y: ry } = *r_center;
    let half_width = r_width / 2.0;
    let half_height = r_height / 2.0;
    let is_c_center_inside_r = f32::abs(cx - rx) <= half_width
        && f32::abs(cy - ry) <= half_height;
    if is_c_center_inside_r {
        return true;
    }
    let test_x = if cx < rx - half_width { rx - half_width }
        else if cx > rx + half_width { rx + half_width }
        else { cx };
    let test_y = if cy < ry - half_height { ry - half_height }
        else if cy > ry + half_height { ry + half_height }
        else { cy };
    Point2D { x: test_x, y: test_y }.distance_to(c_center) <= c_radius
}

// (l, r, t, b)
fn get_rect_bounds(center: &Point2D, width: f32, height: f32) -> (f32, f32, f32, f32) {
    let left = center.x - width / 2.0;
    let right = left + width;
    let top = center.y - height / 2.0;
    let bot = top + height;
    (left, right, top, bot)
}

pub fn do_rectangles_collide(
    r1_center: &Point2D,
    r1_width: f32,
    r1_height: f32,
    r2_center: &Point2D,
    r2_width: f32,
    r2_height: f32,
) -> bool {
    let (r1_left, r1_right, r1_top, r1_bot) = get_rect_bounds(r1_center, r1_width, r1_height);
    let (r2_left, r2_right, r2_top, r2_bot) = get_rect_bounds(r2_center, r2_width, r2_height);
    r1_left < r2_right
        && r2_left < r1_right
        && r1_top < r2_bot
        && r2_top < r1_bot
}