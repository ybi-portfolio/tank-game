use actix::Message;
use serde::{Serialize, Deserialize};

// TODO - maybe use nalgebra
#[derive(Message, Debug, Serialize, Deserialize, Clone, Copy)]
#[rtype(result = "()")]
pub struct Point2D {
    pub x: f32,
    pub y: f32
}

#[derive(Debug, Serialize, Clone, Copy)]
pub struct Vector2D {
    pub x: f32,
    pub y: f32
}

impl Vector2D {
    pub fn new(x: f32, y: f32) -> Vector2D {
        Vector2D { x, y }
    }

    pub fn between_points(from: &Point2D, to: &Point2D) -> Vector2D {
        Vector2D { x: to.x - from.x, y: to.y - from.y }
    }
    
    pub fn len(self: &Vector2D) -> f32 {
        f32::sqrt(self.x * self.x + self.y * self.y)
    }
    
    pub fn mul_scalar(self: &Vector2D, coef: f32) -> Vector2D {
        Vector2D { x: self.x * coef, y: self.y * coef }
    }

    pub fn normalized(self: &Vector2D) -> Vector2D {
        // TODO - fast inverse square root
        let inverse_len = 1.0 / self.len();
        self.mul_scalar(inverse_len)
    }

    pub fn inverted(self: &Vector2D) -> Vector2D {
        self.mul_scalar(-1.0)
    }
}

impl Point2D {
    pub fn new(x: f32, y: f32) -> Point2D {
        Point2D { x, y }
    }

    pub fn translate(self: &mut Point2D, vector: &Vector2D) {
        self.x += vector.x;
        self.y += vector.y;
    }

    pub fn translated(self: &Point2D, vector: &Vector2D) -> Point2D {
        Point2D { x: self.x + vector.x, y: self.y + vector.y }
    }

    pub fn distance_to(&self, other: &Point2D) -> f32 {
        Vector2D::between_points(self, other).len()
    }
}

#[cfg(test)]
mod tests {
    // TODO - floating point comparison
    mod vector_tests {
        use crate::algebra::{Vector2D, Point2D};

        #[test]
        fn vector_between_different_points() {
            let vector = Vector2D::between_points(
                &Point2D::new(1.0, 0.0),
                &Point2D::new(0.0, 2.0)
            );
            assert_eq!(vector.x, -1.0);
            assert_eq!(vector.y, 2.0);
        }
    
        #[test]
        fn vector_between_equal_points() {
            let vector = Vector2D::between_points(
                &Point2D::new(1.0, 3.0),
                &Point2D::new(1.0, 3.0)
            );
            assert_eq!(vector.x, 0.0);
            assert_eq!(vector.y, 0.0);
        }
        
        #[test]
        fn len_of_non_zero_vector() {
            let vector = Vector2D::new(3.0, 4.0);
            assert_eq!(vector.len(), 5.0);
        }
        
        #[test]
        fn len_of_zero_vector() {
            let vector = Vector2D::new(0.0, 0.0);
            assert_eq!(vector.len(), 0.0);
        }
    
        #[test]
        fn mul_scalar() {
            let vector = Vector2D::new(2.0, 3.0);
            let scaled = vector.mul_scalar(3.0);
            assert_eq!(scaled.x, 6.0);
            assert_eq!(scaled.y, 9.0);
        }
    
        #[test]
        fn normalized_when_bigger() {
            let vector = Vector2D::new(3.0, 4.0);
            let normalized = vector.normalized();
            assert_eq!(normalized.x, 0.6);
            assert_eq!(normalized.y, 0.8);
        }
    
        #[test]
        fn normalized_when_smaller() {
            let vector = Vector2D::new(0.3, 0.4);
            let normalized = vector.normalized();
            assert_eq!(normalized.x, 0.6);
            assert_eq!(normalized.y, 0.8);
        }
    
        #[test]
        fn inverted_when_non_zero() {
            let vector = Vector2D::new(3.0, 4.0);
            let inverted = vector.inverted();
            assert_eq!(inverted.x, -3.0);
            assert_eq!(inverted.y, -4.0);
        }
    
        #[test]
        fn inverted_when_zero() {
            let vector = Vector2D::new(0.0, 0.0);
            let inverted = vector.inverted();
            assert_eq!(inverted.x, 0.0);
            assert_eq!(inverted.y, 0.0);
        }
    }
    
    mod point_tests {
        use crate::algebra::{Point2D, Vector2D};
        
        #[test]
        fn translated() {
            let point = Point2D::new(3.0, 4.0);
            let translated_point = point.translated(
                &Vector2D::new(3.0, 2.0));
            assert_eq!(translated_point.x, 6.0);
            assert_eq!(translated_point.y, 6.0);
        }

        #[test]
        fn distance_to() {
            let from = Point2D::new(3.0, 0.0);
            let to = Point2D::new(0.0, 4.0);
            let d1 = from.distance_to(&to);
            let d2 = to.distance_to(&from);
            assert_eq!(d1, 5.0);
            assert_eq!(d2, 5.0);
        }
    }
}