use actix::prelude::{Message, Recipient};
use serde::{Deserialize, Serialize};

use crate::algebra::{Point2D, Vector2D};

#[derive(Message, Clone, Serialize, Debug)]
#[rtype(result = "()")]
#[serde(tag = "event_type", content = "payload")]
pub enum GameEvent {
    Identity {
        player_id: usize
    },
    GameUpdate {
        tanks: Vec<TankView>,
        bullets: Vec<Bullet>,
        walls: Vec<RectWall>,
        timestamp: u128
    }
}

#[derive(Clone, Deserialize, Debug)]
#[serde(tag = "event_type", content = "payload")]
pub enum PlayerCommand {
    MoveTo {
        target: Point2D
    },
    ShootAt {
        target: Point2D
    }
}

#[derive(Message, Clone, Debug)]
#[rtype(result = "()")]
pub struct PlayerCommandWithIdentity {
    pub player_id: usize,
    pub command: PlayerCommand
}

#[derive(Message)]
#[rtype(usize)]
pub struct Connect {
    pub subscriber: Recipient<GameEvent>,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub session_id: usize,
}

#[derive(Debug, Clone)]
pub struct Tank {
    pub player_id: usize,
    pub pos: Point2D,
    pub target: Option<Point2D>,
    pub shooting_cooldown_expiry: u128,
    pub health: u8,
    pub points: u16,
    pub invincible_until: u128,
    pub name: String
}

#[derive(Debug, Serialize, Clone)]
pub struct TankView {
    pub player_id: usize,
    pub pos: Point2D,
    pub target: Option<Point2D>,
    pub health: u8,
    pub points: u16,
    pub is_invincible: bool,
    pub name: String
}

#[derive(Message, Debug, Serialize, Clone, Copy)]
#[rtype(result = "()")]
pub struct Bullet {
    pub player_id: usize,
    pub pos: Point2D,
    pub direction: Vector2D
}

#[derive(Message, Debug, Serialize, Clone, Copy)]
#[rtype(result = "()")]
pub struct RectWall {
    pub pos: Point2D,
    pub width: f32,
    pub height: f32
}

impl RectWall {
    pub fn new(pos: Point2D, width: f32, height: f32) -> RectWall {
        RectWall { pos, width, height }
    }
}
