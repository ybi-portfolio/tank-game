use std::{time::Duration, cmp::Ordering};

use actix::{Addr, Actor, Context, Handler, AsyncContext, WrapFuture, ActorFutureExt, ActorContext, fut, ContextFutureSpawner, Running};
use rand::{rngs::ThreadRng, Rng};
use crate::{server::{self, GAME_MAP_WIDTH, GAME_MAP_HEIGHT}, dto::{GameEvent, Connect, Disconnect, PlayerCommandWithIdentity, TankView}, algebra::{Point2D, Vector2D}};

const BOT_INACCURACY: f32 = 20.0;

pub struct TankBot {
    pub session_id: Option<usize>,
    pub server: Addr<server::GameServer>,
    pub rng: ThreadRng,
    pub is_movement_planned: bool,
    pub should_shoot: bool
}

impl TankBot {
    pub fn new(server: Addr<server::GameServer>) -> TankBot {
        TankBot {
            session_id: None,
            server,
            rng: rand::thread_rng(),
            is_movement_planned: false,
            should_shoot: true
        }
    }

    fn move_randomly(&mut self, ctx: &mut Context<Self>) {
        self.is_movement_planned = true;
        ctx.run_later(Duration::from_secs(1), |act, _| {
            let target = Point2D {
                x: act.rng.gen::<f32>() * GAME_MAP_WIDTH,
                y: act.rng.gen::<f32>() * GAME_MAP_HEIGHT
            };
            if let Some(session_id) = act.session_id {
                act.server.do_send(PlayerCommandWithIdentity {
                    player_id: session_id,
                    command: crate::dto::PlayerCommand::MoveTo { target },
                });
                act.is_movement_planned = false;
            }
        });
    }

    fn get_position_of_nearest_tank(me: &TankView, other_tanks: &[&TankView]) -> Option<Point2D> {
        other_tanks.iter()
            .map(|tank| {
                (tank, tank.pos.distance_to(&me.pos))
            })
            .min_by(|(_, d1), (_, d2)| {
                d1.partial_cmp(d2).unwrap_or(Ordering::Equal)
            })
            .map(|(tank, _)| tank.pos)
    }

    fn get_random_aim_offset_vector(rng: &mut ThreadRng) -> Vector2D {
        let x = rng.gen_range(-BOT_INACCURACY..BOT_INACCURACY);
        let y = rng.gen_range(-BOT_INACCURACY..BOT_INACCURACY);
        Vector2D::new(x, y)
    }
}

impl Actor for TankBot {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let addr = ctx.address();
        self.server
            .send(Connect {
                subscriber: addr.recipient(),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(response) => {
                        act.session_id = Some(response);
                    },
                    _ => {
                        log::warn!("Server couldn't add bot");
                        ctx.stop();
                    }
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        if let Some(session_id) = self.session_id {
            self.server.do_send(Disconnect { session_id });
        }
        Running::Stop
    }
}

impl Handler<GameEvent> for TankBot {
    type Result = ();

    fn handle(&mut self, game_event: GameEvent, ctx: &mut Self::Context) {
        if let Some(session_id) = self.session_id {
            match game_event {
                GameEvent::Identity { player_id: _ } => {
                    log::warn!("Unexpected identity event from server");
                },
                GameEvent::GameUpdate {
                    tanks,
                    bullets: _,
                    walls: _,
                    timestamp: _
                } => {
                    let maybe_me = tanks.iter()
                        .find(|tank| tank.player_id == session_id);
                    if let Some(me) = maybe_me {
                        if me.target.is_none() && !self.is_movement_planned {
                            self.move_randomly(ctx);
                        }
                        if self.should_shoot {
                            let not_me: Vec<&TankView> = tanks.iter()
                                .filter(|tank| tank.player_id != session_id).collect();
                            let target = Self::get_position_of_nearest_tank(me, &not_me)
                                .map(|pos| {
                                    pos.translated(&Self::get_random_aim_offset_vector(&mut self.rng))
                                });
                            if let Some(target) = target {
                                self.should_shoot = false;
                                ctx.run_later(Duration::from_secs(1), |act, _| {
                                    act.should_shoot = true;
                                });
                                self.server.do_send(PlayerCommandWithIdentity {
                                    player_id: session_id,
                                    command: crate::dto::PlayerCommand::ShootAt { target },
                                });
                            }
                        }
                    } else {
                        ctx.stop();
                    }
                },
            }
        }
    }
}