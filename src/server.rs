use std::{
    collections::HashMap, time::Duration,
};

use actix::{prelude::{Actor, Context, Handler, Recipient}, AsyncContext};
use rand::{self, rngs::ThreadRng, Rng};
use names::Generator;

use crate::{
    dto::{
        Connect,
        Disconnect,
        PlayerCommand,
        Tank,
        GameEvent,
        PlayerCommandWithIdentity,
        Bullet,
        RectWall,
        TankView
    },
    algebra::{
        Point2D,
        Vector2D
    },
    util::{
        get_epoch_time,
        does_circle_collide_with_rect,
        do_rectangles_collide
    }, bot::TankBot
};

const TANK_WIDTH: f32 = 20.0;
const BULLET_RADIUS: f32 = 5.0;
const BULLET_BASE_DAMAGE: u8 = 10;
const TANK_BASE_SPEED: f32 = 150.0;
const TANK_BASE_HEALTH: u8 = 100;
const TANK_SHOOTING_COOLDOWN_MILLIS: u128 = 300;
const INVINCIBILITY_TIME_ON_JOIN: u128 = 4000;
const WALL_KNOCKBACK_DISTANCE: f32 = 20.0;
const BULLET_SPEED: f32 = 300.0;
const BOT_SPAWN_THRESHOLD: usize = 8;
pub const GAME_MAP_WIDTH: f32 = 1280.0;
pub const GAME_MAP_HEIGHT: f32 = 720.0;

pub struct GameServer {
    sessions: HashMap<usize, Recipient<GameEvent>>,
    rng: ThreadRng,
    tanks: Vec<Tank>,
    bullets: Vec<Bullet>,
    walls: Vec<RectWall>,
    now: u128,
    name_generator: Generator<'static>
}

impl GameServer {
    pub fn new() -> GameServer {
        GameServer {
            sessions: HashMap::new(),
            rng: rand::thread_rng(),
            tanks: vec![],
            bullets: vec![],
            walls: vec![
                RectWall::new(
                    Point2D::new(0.0, 360.0),
                    20.0,
                    720.0
                ),
                RectWall::new(
                    Point2D::new(1280.0, 360.0),
                    20.0,
                    720.0
                ),
                RectWall::new(
                    Point2D::new(640.0, 0.0),
                    1280.0,
                    20.0
                ),
                RectWall::new(
                    Point2D::new(640.0, 720.0),
                    1280.0,
                    20.0
                ),
                RectWall::new(
                    Point2D::new(220.0, 160.0),
                    30.0,
                    100.0
                ),
                RectWall::new(
                    Point2D::new(170.0, 225.0),
                    130.0,
                    30.0
                ),
                RectWall::new(
                    Point2D::new(1020.0, 460.0),
                    30.0,
                    100.0
                ),
                RectWall::new(
                    Point2D::new(1070.0, 395.0),
                    130.0,
                    30.0
                ),
                RectWall::new(
                    Point2D::new(750.0, 70.0),
                    30.0,
                    140.0
                ),
                RectWall::new(
                    Point2D::new(500.0, 650.0),
                    30.0,
                    140.0
                ),
                RectWall::new(
                    Point2D::new(640.0, 360.0),
                    40.0,
                    40.0
                )
            ],
            now: get_epoch_time(),
            name_generator: Generator::default()
        }
    }

    fn broadcast_game_update(&self) {
        let tanks: Vec<TankView> = self.tanks.iter()
            .map(|tank| Self::map_to_tank_view(tank, self.now))
            .collect();
        for recipient in self.sessions.values() {
            // TODO - maybe RC
            recipient.do_send(GameEvent::GameUpdate {
                tanks: tanks.clone(),
                bullets: self.bullets.clone(),
                walls: self.walls.clone(),
                timestamp: self.now
            });
        }
    }

    fn get_new_player_pos(rng: &mut ThreadRng, walls: &[RectWall]) -> Point2D {
        loop {
            let candidate = Point2D {
                x: rng.gen::<f32>() * GAME_MAP_WIDTH,
                y: rng.gen::<f32>() * GAME_MAP_HEIGHT
            };
            let collides_with_wall = walls.iter()
                .any(|wall| Self::does_tank_collide_with_wall(&candidate, wall));
            if !collides_with_wall {
                return candidate;
            }
        }
    }

    fn move_tanks(&mut self, delta_time: f32) {
        for tank in &mut self.tanks {
            Self::move_tank(tank, &self.walls, delta_time);
        }
    }

    fn move_bullets(&mut self, delta_time: f32) {
        for bullet in &mut self.bullets {
            Self::move_bullet(bullet, delta_time);
        }
        self.bullets.retain(|bullet| Self::is_inside_map(&bullet.pos));
    }

    fn collide_bullets_with_walls(&mut self) {
        self.bullets.retain(|bullet| {
            !self.walls.iter().any(|wall| {
                does_circle_collide_with_rect(
                    &bullet.pos,
                    BULLET_RADIUS,
                    &wall.pos,
                    wall.width,
                    wall.height
                )
            })
        });
    }

    fn find_tank(tanks: &mut [Tank], player_id: usize) -> Option<&mut Tank> {
        tanks.iter_mut().find(|candidate| candidate.player_id == player_id)
    }

    fn collide_bullets_with_tanks(&mut self) {
        let mut hits: Vec<(usize, usize)> = vec![];
        self.bullets.retain(|bullet| {
            !self.tanks.iter()
            .filter(|tank| tank.player_id != bullet.player_id)
            .any(|tank| {
                let should_damage = Self::does_bullet_collide_with_tank(bullet, tank);
                if should_damage {
                    hits.push((bullet.player_id, tank.player_id));
                }
                should_damage
            })
        });

        for (killer_id, killed_id) in hits {
            if let Some(tank) = Self::find_tank(&mut self.tanks, killed_id) {
                if self.now <= tank.invincible_until {
                    continue;
                }
                tank.health = tank.health.saturating_sub(BULLET_BASE_DAMAGE);
                if tank.health == 0 {
                    let killed_points = tank.points;
                    if let Some(killer) = Self::find_tank(&mut self.tanks, killer_id) {
                        killer.health = TANK_BASE_HEALTH;
                        killer.points += killed_points;
                    }
                }
            }
        }
        self.tanks.retain(|tank| tank.health > 0);
    }

    fn update_game(&mut self, delta_time: f32) {
        self.move_tanks(delta_time);
        self.move_bullets(delta_time);
        self.collide_bullets_with_walls();
        self.collide_bullets_with_tanks();
    }

    fn move_tank(tank: &mut Tank, walls: &[RectWall], delta_time: f32) {
        if let Some(target) = &tank.target {
            let distance_to_target = Vector2D::between_points(&tank.pos, target);
            let step = TANK_BASE_SPEED * delta_time;
            if distance_to_target.len() < step {
                tank.pos = *target;
                tank.target = None;
            } else {
                let movement_vec = distance_to_target.normalized()
                    .mul_scalar(step);
                let old_pos = tank.pos;
                tank.pos.translate(&movement_vec);
                let collides_with_wall = walls.iter()
                    .any(|wall| Self::does_tank_collide_with_wall(&tank.pos, wall));
                if collides_with_wall {
                    tank.pos = old_pos;
                    let knockback_vector = &movement_vec
                        .inverted()
                        .normalized()
                        .mul_scalar(WALL_KNOCKBACK_DISTANCE);
                    tank.target = Some(old_pos.translated(knockback_vector));
                }
            }
        }
    }

    fn move_bullet(bullet: &mut Bullet, delta_time: f32) {
        bullet.pos.translate(&bullet.direction
            .mul_scalar(delta_time * BULLET_SPEED));
    }

    fn is_inside_map(pos: &Point2D) -> bool {
        0.0 <= pos.x && pos.x <= GAME_MAP_WIDTH
        && 0.0 <= pos.y && pos.y <= GAME_MAP_HEIGHT
    }

    fn does_bullet_collide_with_tank(bullet: &Bullet, tank: &Tank) -> bool {
        does_circle_collide_with_rect(
            &bullet.pos,
            BULLET_RADIUS,
            &tank.pos,
            TANK_WIDTH,
            TANK_WIDTH
        )
    }

    fn does_tank_collide_with_wall(tank_pos: &Point2D, wall: &RectWall) -> bool {
        do_rectangles_collide(
            tank_pos,
            TANK_WIDTH,
            TANK_WIDTH,
            &wall.pos,
            wall.width,
            wall.height
        )
    }

    fn try_shoot(tank: &mut Tank, target: Point2D) -> Option<Bullet> {
        let now = get_epoch_time();
        let can_shoot = tank.shooting_cooldown_expiry <= now;
        if can_shoot {
            let direction = Vector2D::between_points(&tank.pos, &target)
                .normalized();
            let bullet = Bullet {
                player_id: tank.player_id,
                pos: tank.pos, // TODO - barrel offset?
                direction,
            };
            tank.shooting_cooldown_expiry = now + TANK_SHOOTING_COOLDOWN_MILLIS;
            return Some(bullet);
        }
        None
    }

    fn map_to_tank_view(tank: &Tank, now: u128) -> TankView {
        TankView {
            player_id: tank.player_id,
            pos: tank.pos,
            target: tank.target,
            health: tank.health,
            points: tank.points,
            is_invincible: now <= tank.invincible_until,
            name: tank.name.clone()
        }
    }

    fn handle_tick(&mut self) {
        let now = get_epoch_time();
        // the diff should be relatively small
        let delta_time = (now - self.now) as f32 / 1000.0;
        self.now = now;

        self.update_game(delta_time);
        self.broadcast_game_update();
    }
}

impl Actor for GameServer {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(Duration::from_millis(33), |act, _| {
            act.handle_tick();
        });

        ctx.run_interval(Duration::from_secs(5), |act, ctx| {
            if act.tanks.len() <= BOT_SPAWN_THRESHOLD {
                TankBot::new(ctx.address())
                    .start();
            }
        });
    }
}

impl Handler<Connect> for GameServer {
    type Result = usize;

    fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
        let session_id = self.rng.gen::<usize>();
        self.sessions.insert(session_id, msg.subscriber);
        let player_position = Self::get_new_player_pos(&mut self.rng, &self.walls);
        
        let now = get_epoch_time();
        let invincible_until = now + INVINCIBILITY_TIME_ON_JOIN;
        let name= self.name_generator.next()
            .unwrap_or(format!("Player {}", self.rng.gen::<u8>()));
        self.tanks.push(Tank {
            pos: player_position,
            target: None,
            player_id: session_id,
            shooting_cooldown_expiry: invincible_until,
            health: TANK_BASE_HEALTH,
            points: 1,
            invincible_until,
            name
        });

        session_id
    }
}

impl Handler<Disconnect> for GameServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        self.tanks.retain(|e| e.player_id != msg.session_id);
        self.sessions.remove(&msg.session_id);
    }
}

impl Handler<PlayerCommandWithIdentity> for GameServer {
    type Result = ();

    fn handle(&mut self, event: PlayerCommandWithIdentity, _: &mut Context<Self>) {
        if let Some(tank) = Self::find_tank(&mut self.tanks, event.player_id) {
            match event.command {
                PlayerCommand::MoveTo { target } => {
                    tank.target = Some(target);
                },
                PlayerCommand::ShootAt { target } => {
                    if let Some(bullet) = Self::try_shoot(tank, target) {
                        self.bullets.push(bullet);
                    }
                }
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use actix::{Handler, Actor, Context};
    use crate::{dto::{Connect, GameEvent, Tank, Bullet}, server::TANK_BASE_HEALTH, algebra::{Point2D, Vector2D}};
    use super::GameServer;
    struct GameEventHandler {
        received: Vec<GameEvent>
    }
    impl GameEventHandler {
        pub fn new() -> GameEventHandler {
            GameEventHandler { received: vec![] }
        }
    }
    impl Actor for GameEventHandler {
        type Context = Context<Self>;
    }
    impl Handler<GameEvent> for GameEventHandler {
        type Result = ();
    
        fn handle(&mut self, game_event: GameEvent, _: &mut Self::Context) {
            self.received.push(game_event);
        }
    }
    fn get_tank() -> Tank {
         Tank {
            pos: Point2D::new(120.0, 120.0),
            player_id: 123,
            target: None,
            shooting_cooldown_expiry: 123,
            health: 100,
            points: 1,
            invincible_until: 123,
            name: "name".to_owned(),
        }
    }

    #[actix::test]
    async fn should_add_tank_on_join() {
        let mut server = GameServer::new();
        let subscriber = GameEventHandler::new();
        let recepient = subscriber.start().recipient();
        let session_id = server.handle(Connect {
            subscriber: recepient
        }, &mut Context::new());

        assert_eq!(server.tanks.len(), 1);
        assert_eq!(server.tanks[0].health, TANK_BASE_HEALTH);
        assert_eq!(server.tanks[0].player_id, session_id);
        assert_eq!(server.tanks[0].points, 1);
    }

    #[test]
    fn should_collide_tank_with_bullet() {
        let mut server = GameServer::new();
        server.tanks.push(get_tank());
        server.bullets.push(Bullet {
            player_id: 321,
            pos: Point2D::new(121.0, 122.0),
            direction: Vector2D::new(0.0, 1.0)
        });
        server.collide_bullets_with_tanks();
        assert_eq!(server.tanks.len(), 1);
        assert_eq!(server.tanks[0].health, 90);
        assert_eq!(server.bullets.len(), 0);
    }

    #[test]
    fn should_kill_player_when_hit_and_low_on_health() {
        let mut server = GameServer::new();
        server.tanks.push(get_tank());
        server.tanks[0].health = 10;
        server.bullets.push(Bullet {
            player_id: 321,
            pos: Point2D::new(121.0, 122.0),
            direction: Vector2D::new(0.0, 1.0)
        });
        server.collide_bullets_with_tanks();
        assert_eq!(server.tanks.len(), 0);
        assert_eq!(server.bullets.len(), 0);
    }

    #[test]
    fn should_restore_killer_hp_upon_kill() {
        let mut server = GameServer::new();
        server.tanks.push(get_tank());
        server.tanks.push(get_tank());
        server.tanks[1].player_id = 321;
        server.tanks[1].health = 60;
        server.tanks[0].health = 10;
        server.bullets.push(Bullet {
            player_id: 321,
            pos: Point2D::new(121.0, 122.0),
            direction: Vector2D::new(0.0, 1.0)
        });
        server.collide_bullets_with_tanks();
        assert_eq!(server.tanks.len(), 1);
        assert_eq!(server.tanks[0].player_id, 321);
        assert_eq!(server.tanks[0].health, 100);
        assert_eq!(server.tanks[0].points, 2);
        assert_eq!(server.bullets.len(), 0);
    }
}