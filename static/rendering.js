const $canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const palette = [
    "#73464C",
    "#72DCBB",
    "#EE6A7C",
    "#AB5675",
    "#FFE07E"
];
const [backgroundColor, playerColor, enemyColor, healthColor, wallColor] = palette;

const getPlayerColor = (playerId) => {
    return meId === playerId ? playerColor : enemyColor;
}

const resetCanvas = () => {
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, 1280, 720);
}

const renderRect = (x, y, width, height, color) => {
    ctx.fillStyle = color;
    ctx.fillRect(x - width / 2, y - height / 2, width, height);
}

const renderWalls = (walls) => {
    walls.forEach(({pos: {x, y}, height, width}) => {
        renderRect(x, y, width, height, wallColor);
    })
}

const renderTanks = (tanks) => {
    let mostPoints = tanks
        .map((t) => t.points)
        .reduce((a, b) => Math.max(a, b), 0);
    tanks.forEach(({
        player_id,
        pos: {x, y},
        health,
        points,
        is_invincible,
        name
    }) => {
        let color = getPlayerColor(player_id);
        let isKing = mostPoints === points;
        if (is_invincible) {
            renderRect(x, y, 26, 26, wallColor);
        }
        renderRect(x, y, 20, 20, color);
        renderRect(x, y + 20, health / 2, 4, healthColor);
        ctx.font = "1rem arial";
        ctx.fillStyle = "white";
        ctx.textAlign = "center";
        if (isKing) {
            ctx.fillStyle = "orange";
        }
        ctx.fillText(points, x, y - 20);
        let namePrefix = isKing ? "👑": "";
        ctx.fillText(namePrefix + name, x, y - 40);
    });
}

const renderBullets = (bullets) => {
    bullets.forEach(bullet => {
        ctx.fillStyle = getPlayerColor(bullet.player_id);
        ctx.beginPath();
        ctx.arc(bullet.pos.x, bullet.pos.y, 5, 0, 2 * Math.PI);
        ctx.fill();
    });
}

const renderFrame = (gameState) => {
    resetCanvas();
    renderBullets(gameState.bullets);
    renderWalls(gameState.walls);
    renderTanks(gameState.tanks);
}