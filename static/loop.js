const TARGET_FPS = 165;
setInterval(() => {
    if (!!lastSnapshot) {
        let now = new Date().getTime();
        renderFrame(extrapolateGameState(now));
    }
}, 1000 / TARGET_FPS);