const getClickPos = (event) => {
    const rect = $canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    return { x, y };
};

const handleRightClick = ({ x, y }) => {
    queuedMovement = {
        x,
        y,
        timestamp: new Date().getTime()
    };
    sendEvent(getMoveToEvent(x, y));
};

const handleClick = ({ x, y }) => {
    sendEvent(getShootAtEvent(x, y));
};
