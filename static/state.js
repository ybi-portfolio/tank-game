let meId = null;
let lastSnapshot = null;
let queuedMovement = null;

let BULLET_SPEED = 300;
let TANK_SPEED = 150;

let extrapolateGameState = (now) => {
    let secondsSince = (now - lastSnapshot.timestamp) / 1000;
    let extrapolatedBullets = lastSnapshot.bullets
        .map((bullet) => {
            let newX = bullet.pos.x + bullet.direction.x * BULLET_SPEED * secondsSince;
            let newY = bullet.pos.y + bullet.direction.y * BULLET_SPEED * secondsSince;
            return {
                ...bullet,
                pos: {
                    x: newX,
                    y: newY
                }
            }
        });
    let extrapolatedtanks = lastSnapshot.tanks
        .map((tank) => {
            let target = tank.target;
            if (tank.player_id === meId && !!queuedMovement) {
                target = queuedMovement;
            }
            if (!tank.target) {
                return tank;
            }
            let distanceX = target.x - tank.pos.x;
            let distanceY = target.y - tank.pos.y;
            let distanceLen = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
            let dirX = distanceX / distanceLen;
            let dirY = distanceY / distanceLen;
            let newX = tank.pos.x + dirX * TANK_SPEED * secondsSince;
            let newY = tank.pos.y + dirY * TANK_SPEED * secondsSince;
            return {
                ...tank,
                pos: {
                    x: newX,
                    y: newY
                }
            };
        })
    let extrapolatedGameState = {
        ...lastSnapshot,
        tanks: extrapolatedtanks,
        bullets: extrapolatedBullets,
    };
    return extrapolatedGameState;
}