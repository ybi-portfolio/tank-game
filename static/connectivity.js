const $status = document.querySelector('#status');
let socket = null;

const getMoveToEvent = (x, y) => {
    return {
        event_type: "MoveTo",
        payload: { target: { x, y } }
    }
}

const getShootAtEvent = (x, y) => {
    return {
        event_type: "ShootAt",
        payload: { target: { x, y } }
    }
}

const sendEvent = (event) => {
    socket.send(JSON.stringify(event));
}

const onMessage = (msg) => {
    switch (msg.event_type) {
        case "GameUpdate":
            handleGameUpdate(msg.payload);
            break;
        case "Identity":
            meId = msg.payload.player_id;
            break;
    }
};

const connect = () => {
    disconnect()

    const { location } = window

    const proto = location.protocol.startsWith('https') ? 'wss' : 'ws'
    const wsUri = `${proto}://${location.host}/ws`

    socket = new WebSocket(wsUri)

    socket.onopen = () => {
        updateConnectionStatus()
    }

    socket.onmessage = (ev) => {
        let data = JSON.parse(ev.data);
        onMessage(data);
    }

    socket.onclose = () => {
        socket = null
        updateConnectionStatus()
    }
};

const disconnect = () => {
    if (socket) {
        socket.close()
        socket = null
        updateConnectionStatus()
    }
}

const updateConnectionStatus = () => {
    if (socket) {
        $status.style.backgroundColor = 'transparent'
        $status.style.color = 'green'
        $status.textContent = `connected`
    } else {
        $status.style.backgroundColor = 'red'
        $status.style.color = 'white'
        $status.textContent = 'disconnected'
    }
}
updateConnectionStatus()
connect()
$canvas.addEventListener("contextmenu", (e) => {
    e.preventDefault();
    handleRightClick(getClickPos(e))
});
$canvas.addEventListener("click", (e) => {
    e.preventDefault();
    handleClick(getClickPos(e))
});