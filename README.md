## Introduction
My final project for the [Rust course at Sofia Univeristy](https://fmi.rust-lang.bg/).  
It represents a multiplayer tank game using websockets for communication.  
The backend is in `rust` and `actix`.  
The frontend is vanilla `HTML` and `JavaScript`, using the `canvas` and `WebSocket` APIs.  
The app uses [client-side prediction](https://en.wikipedia.org/wiki/Client-side_prediction) to ensure smoother user experience even with higher network latency.

## Gameplay
Each player is a tank, starting with 1 point and full HP, and a short invincibility shield.  
The game features bots that move and shoot in a not-so-smart way.  
Whenever a player kills another, it gains all of its points and gains full HP once again.  
The player with the most points is the king, making them an easy target for the rest of the players to focus.  
Use `right click` to move and `left click` to shot.
![gameplay](docs/gameplay.png)

## Demo
![demo video](docs/demo.mp4)

## Run it yourself
```shell
docker build -t tank_game .
docker run -p 8080:8080 tank_game
# open localhost:8080 in your browser
```